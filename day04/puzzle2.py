import sys

class Cell:
    def __init__(self, number):
        self.number = number
        self.called = False
    def __repr__(self):
        if int(self.number) < 10:
            returned_number = f" {self.number}"
        else:
            returned_number = self.number
        if self.called:
            return f"*{returned_number}*"
        else:
            return f" {returned_number} "
    def mark_called(self):
        self.called = True

class Board:
    def __init__(self):
        self.rows = []
    def add_row(self, numbers):
        current_row = []
        for n in numbers.strip().split():
            current_row.append(Cell(n))
        self.rows.append(current_row)
    def is_full(self):
        return len(self.rows) > 4
    def __repr__(self):
        output = ""
        for row in self.rows:
            for cell in row:
                output += str(cell)
            output += "\n"
        return output
    def mark_number(self, number):
        for row in self.rows:
            for cell in row:
                if int(cell.number) == int(number):
                    cell.mark_called()
    def has_bingo(self):
        for row in self.rows:
            row_marks = []
            for cell in row:
                if cell.called:
                    row_marks.append(cell)
            if len(row_marks) == 5:
                return True
        columns = {}
        for row in self.rows:
            for col, cell in enumerate(row):
                if col not in columns:
                    columns.update({col: []})
                columns[col].append(cell)
        for col in columns:
            col_marks = []
            for cell in columns[col]:
                if cell.called:
                    col_marks.append(cell)
            if len(col_marks) == 5:
                return True
        return False
    def sum_unmarked(self):
        total = 0
        for row in self.rows:
            for cell in row:
                if not cell.called:
                    total += int(cell.number)
        return total


class Caller:
    def __init__(self, numbers):
        self.numbers = []
        for n in numbers.strip().split(","):
            self.numbers.append(n)
    def call_number(self):
        return self.numbers.pop(0)
    def not_empty(self):
        return len(self.numbers) > 0
    def game_over(self):
        self.numbers = []


with open("input.txt") as infile:
    boards = []
    current_board = Board()
    for line in infile.readlines():
        if line.strip():
            if "," in line:
                sub_caller = Caller(line)
            else:
                current_board.add_row(line)
                if current_board.is_full():
                    boards.append(current_board)
                    current_board = Board()
    winning_board_numbers = []
    numbers_called = []
    while sub_caller.not_empty():
        called_number = sub_caller.call_number()
        numbers_called.append(called_number)
        print(f"Number {called_number} was drawn.")
        for number, board in enumerate(boards):
            if number not in winning_board_numbers:
                board.mark_number(called_number)
                if board.has_bingo():
                    print(board)
                    print(f"BINGO on board {number + 1}")
                    winning_board_numbers.append(number)
            if len(boards) == len(winning_board_numbers):
                sub_caller.game_over()
                break
    last_board = boards[winning_board_numbers[-1]]
    print(f"Board {winning_board_numbers[-1] + 1} was the last board to win")
    unmarked = last_board.sum_unmarked()
    last_number = numbers_called[-1]
    print(f"Score is {unmarked} * {last_number} = {int(unmarked) * int(last_number)}")

