from termcolor import colored

increasing = []
decreasing = []
depths = []

with open('input.txt') as infile:
    for line in infile.readlines():
        this_depth = int(line.strip())
        color = None
        if depths:
            if depths[-1] < this_depth:
                message = "increased"
                increasing.append(this_depth)
                color = "green"
            else:
                message = "decreased"
                decreasing.append(this_depth)
                color = "red"
        else:
            message = "N/A - no previous measurement"
            color = "blue"
        print(f"{this_depth} ({colored(message, color)})")
        depths.append(this_depth)

print(f"There are {len(increasing)} measurements that are larger than the previous measurement")
print(f"There were {len(decreasing)} decreasing measurements. For a total of {len(depths)} measurements")
