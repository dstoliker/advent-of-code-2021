from termcolor import colored

increasing = []
depths = []
window_sums = []
WINDOW_SIZE = 3

with open("input.txt") as infile:
    for line in infile.readlines():
        this_depth = int(line.strip())
        depths.append(this_depth)
        output = {}
        if len(depths) >= WINDOW_SIZE:
            this_sum = sum(depths[-WINDOW_SIZE:])
            window_sums.append(this_sum)
            output.update({"sum": this_sum})
            if len(window_sums) > 1:
                last_sum = window_sums[-2]
                if this_sum > last_sum:
                    output.update({"message": "increased", "color": "green"})
                    increasing.append(this_sum)
                elif this_sum < last_sum:
                    output.update({"message": "decreased", "color": "red"})
                else:
                    output.update({"message": "no change", "color": "blue"})
            else:
                output.update({"message": "N/A - no previous sum", "color": "blue"})
            print(f"{output.get('sum')} ({colored(output.get('message'), output.get('color'))})")
print(f"There are {len(increasing)} sums that are larger than the previous one")
