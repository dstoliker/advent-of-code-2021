class Fish:
    def __init__(self, timer=9):
        self.timer = int(timer)

    def __repr__(self):
        return str(self.timer)

    def age(self):
        self.timer -= 1
        if self.timer < 0:
            self.timer = 6
            return Fish()
        return None


class School:
    def __init__(self):
        self.school = []
        self.days = 0

    def add_fish(self, fish):
        self.school.append(fish)

    def __repr__(self):
        output = []
        for fish in self.school:
            output.append(str(fish))
        return ",".join(output)

    def day(self):
        self.days += 1
        for fish in self.school:
            result = fish.age()
            if result:
                self.add_fish(result)

    def size(self):
        return len(self.school)


school = School()
with open("input.txt") as infile:
    for line in infile.readlines():
        for timer in line.strip().split(","):
            this_fish = Fish(timer)
            school.add_fish(this_fish)
print(f"{school.days}: {school}")
for day in range(0, 80):
    school.day()
    print(f"{school.days}: {school}")
print(f"There are now {school.size()} fish")
