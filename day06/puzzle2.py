class Fish:
    def __init__(self, timer=9):
        self.timer = int(timer)

    def __repr__(self):
        return str(self.timer)

    def age(self):
        self.timer -= 1
        if self.timer < 0:
            self.timer = 6
            return Fish()
        return None


class School:
    def __init__(self):
        self.school = []
        self.days = 0

    def add_fish(self, fish):
        self.school.append(fish)

    def __repr__(self):
        output = []
        for fish in self.school:
            output.append(str(fish))
        return ",".join(output)

    def day(self):
        self.days += 1
        for fish in self.school:
            result = fish.age()
            if result:
                self.add_fish(result)

    def size(self):
        return len(self.school)


age_lookup = {
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0 }

days = 18
for age in range(1, 6):
    print(f"Calculating total for fish starting at age {age}")
    school = School()
    this_fish = Fish(age)
    school.add_fish(this_fish)
    for day in range(0, days):
        school.day()
        print(f"{school.days}: {school.size()}")
    print(f"There are a total of {school.size()} fish after {days} days for fish starting at age {age}")
    age_lookup.update({age: school.size()})

population = []

print(age_lookup)
with open("test-input.txt") as infile:
    for line in infile.readlines():
        for timer in line.strip().split(","):
            population.append(int(age_lookup[int(timer)]))
print(population)
print(sum(population))
