class Fish:
    def __init__(self, timer, name):
        self.timer = int(timer)
        self.name = name

    def __repr__(self):
        return str(f"{self.name}({self.timer})")

    def age(self):
        self.timer -= 1
        if self.timer < 0:
            self.timer = 6
            return Fish(9, self.name + self.name)
        return None


class School:
    def __init__(self):
        self.school = []
        self.days = 0

    def add_fish(self, fish):
        self.school.append(fish)

    def __repr__(self):
        output = []
        for fish in self.school:
            output.append(str(fish))
        return ",".join(output)

    def day(self):
        self.days += 1
        for fish in self.school:
            result = fish.age()
            if result:
                self.add_fish(result)

    def size(self):
        return len(self.school)

school = School()
afish = Fish(5, 'E')
school.add_fish(afish)
for day in range(1, 19):
    school.day()
    print(f"{school.days}: {school}")
print(school.size())
