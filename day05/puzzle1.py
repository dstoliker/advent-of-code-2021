class Cell:
    def __init__(self):
        self.hits = 0
    def __repr__(self):
        if self.hits == 0:
            return "."
        else:
            return str(self.hits)
    def hit(self):
        self.hits += 1

class Grid:
    def __init__(self, size=10):
        self.grid = []
        for rows in range(0, size):
            current_row = []
            for cell in range(0, size):
                cell = Cell()
                current_row.append(cell)
            self.grid.append(current_row)
    def __repr__(self):
        output = ""
        for row in self.grid:
            for cell in row:
                output += str(cell)
            output += "\n"
        return output
    def record_line(self, points):
        for point in points:
            x, y = point
            self.grid[y][x].hit()
    def get_danger_zones(self, threshold=2):
        score = 0
        for row in self.grid:
            for cell in row:
                if cell.hits >= threshold:
                    score += 1
        return score

def line_to_points(line):
    points = []
    x1y1, x2y2= line.strip().split("->")
    try:
        x1, y1 = x1y1.strip().split(",")
    except ValueError:
        print("Error processing this...")
        print("line")
        print(line)
        print("x1y1")
        print(x1y1)
    try:
        x2, y2 = x2y2.strip().split(",")
    except ValueError:
        print("Error processing this...")
        print("line")
        print(line)
        print("x2y2")
        print(x2y2)
    if x1 == x2:
        for point in range(min(int(y1), int(y2)), (max(int(y1), int(y2)) + 1)):
            points.append((int(x1), int(point)))
    elif y1 == y2:
        for point in range(min(int(x1), int(x2)), (max(int(x1), int(x2)) + 1)):
            points.append((int(point), int(y1)))
    return points


# Making an assumption based on glance at input that values are 0-999
# Would be better to examine input, then make the grid based on max size
grid = Grid(1000)
with open("input.txt") as infile:
    for line in infile.readlines():
        points = line_to_points(line)
        if points:
            grid.record_line(points)
print(grid)
print(f"{grid.get_danger_zones()} points")
