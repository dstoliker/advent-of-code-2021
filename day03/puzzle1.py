positions = {}
readings = 0
gamma = ''
epsilon = ''

with open("input.txt") as infile:
    for line in infile.readlines():
        readings += 1
        binary_value = str(line.strip())
        # Initialize positions
        if not positions:
            for initial_position in range(0, len(binary_value)):
                    positions.update({int(initial_position + 1): 0})
        for position, value in enumerate(binary_value):
            if int(value) == 1:
                current_value = int(positions[position + 1])
                positions.update({position + 1: current_value + int(value)})

for pos in positions:
    ones = int(positions[pos])
    zeros = readings - ones
    if ones > zeros:
        gamma += "1"
        epsilon += "0"
    else:
        gamma += "0"
        epsilon += "1"

gamma_int = int(gamma, 2)
epsilon_int = int(epsilon, 2)

print(f"gamma {gamma} = {gamma_int}")
print(f"epsilon {epsilon} = {epsilon_int}")
print(f"Power consumption is {gamma_int * epsilon_int}")
