def oxygen_rating(readings, position):
    ones = []
    zeros = []
    for line in readings:
        if position > len(line):
            return None
        if line[position] == '1':
            ones.append(line)
        else:
            zeros.append(line)
    if len(ones) >= len(zeros):
        return ones
    else:
        return zeros


def scrubber_rating(readings, position):
    ones = []
    zeros = []
    for line in readings:
        if position > len(line):
            return None
        if line[position] == '1':
            ones.append(line)
        else:
            zeros.append(line)
    if len(ones) < len(zeros):
        return ones
    else:
        return zeros


def read_input(filename='test-input.txt'):
    readings = []
    with open(filename) as infile:
        for line in infile.readlines():
            binary_value = str(line.strip())
            readings.append(binary_value)
    return readings


def bin_to_dec(binary):
    return int(binary, 2)


readings = read_input(filename='input.txt')
oxygen_generator_rating = None
co_scrubber_rating = None

for oxy in range(0,len(readings[0])):
    if oxy == 0:
        oxygen = readings.copy()
    oxygen = oxygen_rating(oxygen, oxy)
    if len(oxygen) == 1:
        oxygen_generator_rating = bin_to_dec(oxygen[0])
        print(f"{oxygen[0]} {oxygen_generator_rating}")
        break

for co in range(0,len(readings[0])):
    if co == 0:
        scrubber = readings.copy()
    scrubber = scrubber_rating(scrubber, co)
    if len(scrubber) == 1:
        co_scrubber_rating = bin_to_dec(scrubber[0])
        print(f"{scrubber[0]} {co_scrubber_rating}")
        break

life_support_rating = oxygen_generator_rating * co_scrubber_rating
print(f"Life Support Rating is {life_support_rating}")
