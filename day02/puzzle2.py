class Submarine:
    def __init__(self):
        self.depth = 0
        self.distance = 0
        self.aim = 0

    def forward(self, amount):
        self.distance += int(amount)
        self.depth += (self.aim * int(amount))

    def up(self, amount):
        self.aim -= int(amount)

    def down(self, amount):
        self.aim += int(amount)

with open("input.txt") as infile:
    sub = Submarine()
    for line in infile.readlines():
        command, amount = line.strip().split()
        if command == 'forward':
            sub.forward(int(amount))
        elif command == 'up':
            sub.up(int(amount))
        elif command == 'down':
            sub.down(int(amount))
        else:
            print(80*"-")
            print(f"Unknown command {command} in input")
            print(80*"-")
        print(f"Submarine position is now {sub.distance} and depth is {sub.depth}")
    print(f"Submarine distance * depth is {sub.distance * sub.depth}")
